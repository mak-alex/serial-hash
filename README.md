#The script for password generation serial numbers of different devices.
-- Input arguments db.xml
--- Format db.xml:
```
#!xml
#
<?xml version="1.0" encoding="utf8"?>
<database>
	<item><serialnum>2131.000.329</serialnum><colors>green</colors></item>
	<!-- or more -->
</database>
```